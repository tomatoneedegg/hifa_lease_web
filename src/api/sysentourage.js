import request from '@/utils/request'

// 查询SysEntourage列表
export function listSysEntourage(query) {
  return request({
    url: '/api/v1/sysentourage',
    method: 'get',
    params: query
  })
}

// 查询SysEntourage详细
export function getSysEntourage(ID) {
  return request({
    url: '/api/v1/sysentourage/' + ID,
    method: 'get'
  })
}

// 新增SysEntourage
export function addSysEntourage(data) {
  return request({
    url: '/api/v1/sysentourage',
    method: 'post',
    data: data
  })
}

// 修改SysEntourage
export function updateSysEntourage(data) {
  return request({
    url: '/api/v1/sysentourage/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysEntourage
export function delSysEntourage(data) {
  return request({
    url: '/api/v1/sysentourage',
    method: 'delete',
    data: data
  })
}

