import request from '@/utils/request'

// 查询SysAssets列表
export function listSysAssets(query) {
  return request({
    url: '/api/v1/sysassets',
    method: 'get',
    params: query
  })
}

// 查询SysAssets详细
export function getSysAssets(ID) {
  return request({
    url: '/api/v1/sysassets/' + ID,
    method: 'get'
  })
}

// 新增SysAssets
export function addSysAssets(data) {
  return request({
    url: '/api/v1/sysassets',
    method: 'post',
    data: data
  })
}

// 修改SysAssets
export function updateSysAssets(data) {
  return request({
    url: '/api/v1/sysassets/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysAssets
export function delSysAssets(data) {
  return request({
    url: '/api/v1/sysassets',
    method: 'delete',
    data: data
  })
}

