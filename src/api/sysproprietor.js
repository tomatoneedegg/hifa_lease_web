import request from '@/utils/request'

// 查询SysProprietor列表
export function listSysProprietor(query) {
  return request({
    url: '/api/v1/sysproprietor',
    method: 'get',
    params: query
  })
}

// 查询SysProprietor详细
export function getSysProprietor(ID) {
  return request({
    url: '/api/v1/sysproprietor/' + ID,
    method: 'get'
  })
}

// 新增SysProprietor
export function addSysProprietor(data) {
  return request({
    url: '/api/v1/sysproprietor',
    method: 'post',
    data: data
  })
}

// 修改SysProprietor
export function updateSysProprietor(data) {
  return request({
    url: '/api/v1/sysproprietor/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysProprietor
export function delSysProprietor(data) {
  return request({
    url: '/api/v1/sysproprietor',
    method: 'delete',
    data: data
  })
}

// 更改状态
export function updateSysProprietorStatus(data) {
  return request({
    url: '/api/v1/status/sysproprietor',
    method: 'put',
    data: data
  })
}

