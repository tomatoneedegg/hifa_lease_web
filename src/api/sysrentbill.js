import request from '@/utils/request'

// 查询SysRentBill列表
export function listSysRentBill(query) {
  return request({
    url: '/api/v1/sysrentbill',
    method: 'get',
    params: query
  })
}

// 查询SysRentBill详细
export function getSysRentBill(ID) {
  return request({
    url: '/api/v1/sysrentbill/' + ID,
    method: 'get'
  })
}

// 新增SysRentBill
export function addSysRentBill(data) {
  return request({
    url: '/api/v1/sysrentbill',
    method: 'post',
    data: data
  })
}

// 修改SysRentBill
export function updateSysRentBill(data) {
  return request({
    url: '/api/v1/sysrentbill/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysRentBill
export function delSysRentBill(data) {
  return request({
    url: '/api/v1/sysrentbill',
    method: 'delete',
    data: data
  })
}

