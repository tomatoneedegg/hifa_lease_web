import request from '@/utils/request'

// 查询SysOtherBill列表
export function listSysOtherBill(query) {
  return request({
    url: '/api/v1/sysotherbill',
    method: 'get',
    params: query
  })
}

// 查询SysOtherBill详细
export function getSysOtherBill(ID) {
  return request({
    url: '/api/v1/sysotherbill/' + ID,
    method: 'get'
  })
}

// 新增SysOtherBill
export function addSysOtherBill(data) {
  return request({
    url: '/api/v1/sysotherbill',
    method: 'post',
    data: data
  })
}

// 修改SysOtherBill
export function updateSysOtherBill(data) {
  return request({
    url: '/api/v1/sysotherbill/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysOtherBill
export function delSysOtherBill(data) {
  return request({
    url: '/api/v1/sysotherbill',
    method: 'delete',
    data: data
  })
}

