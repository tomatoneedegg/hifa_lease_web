import request from '@/utils/request'

// 查询SysCommunity列表
export function listSysCommunity(query) {
  return request({
    url: '/api/v1/syscommunity',
    method: 'get',
    params: query
  })
}

// 查询SysCommunity详细
export function getSysCommunity(ID) {
  return request({
    url: '/api/v1/syscommunity/' + ID,
    method: 'get'
  })
}

// 新增SysCommunity
export function addSysCommunity(data) {
  return request({
    url: '/api/v1/syscommunity',
    method: 'post',
    data: data
  })
}

// 修改SysCommunity
export function updateSysCommunity(data) {
  return request({
    url: '/api/v1/syscommunity/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysCommunity
export function delSysCommunity(data) {
  return request({
    url: '/api/v1/syscommunity',
    method: 'delete',
    data: data
  })
}

