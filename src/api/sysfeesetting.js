import request from '@/utils/request'

// 查询SysFeeSetting列表
export function listSysFeeSetting(query) {
  return request({
    url: '/api/v1/sysfeesetting',
    method: 'get',
    params: query
  })
}

// 查询SysFeeSetting详细
export function getSysFeeSetting(ID) {
  return request({
    url: '/api/v1/sysfeesetting/' + ID,
    method: 'get'
  })
}

// 新增SysFeeSetting
export function addSysFeeSetting(data) {
  return request({
    url: '/api/v1/sysfeesetting',
    method: 'post',
    data: data
  })
}

// 修改SysFeeSetting
export function updateSysFeeSetting(data) {
  return request({
    url: '/api/v1/sysfeesetting/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysFeeSetting
export function delSysFeeSetting(data) {
  return request({
    url: '/api/v1/sysfeesetting',
    method: 'delete',
    data: data
  })
}

