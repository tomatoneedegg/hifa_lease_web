import request from '@/utils/request'

// 查询SysCheck列表
export function listSysCheck(query) {
  return request({
    url: '/api/v1/syscheck',
    method: 'get',
    params: query
  })
}

// 查询SysCheck详细
export function getSysCheck(ID) {
  return request({
    url: '/api/v1/syscheck/' + ID,
    method: 'get'
  })
}

// 新增SysCheck
export function addSysCheck(data) {
  return request({
    url: '/api/v1/syscheck',
    method: 'post',
    data: data
  })
}

// 修改SysCheck
export function updateSysCheck(data) {
  return request({
    url: '/api/v1/syscheck/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysCheck
export function delSysCheck(data) {
  return request({
    url: '/api/v1/syscheck',
    method: 'delete',
    data: data
  })
}

// 退房
export function checkOut(data) {
  return request({
    url: '/api/v1/syscheckout',
    method: 'put',
    data: data
  })
}

