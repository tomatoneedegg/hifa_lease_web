import request from '@/utils/request'

// 查询SysRoom列表
export function listSysRoom(query) {
  return request({
    url: '/api/v1/sysroom',
    method: 'get',
    params: query
  })
}

// 查询SysRoom详细
export function getSysRoom(ID) {
  return request({
    url: '/api/v1/sysroom/' + ID,
    method: 'get'
  })
}

// 新增SysRoom
export function addSysRoom(data) {
  return request({
    url: '/api/v1/sysroom',
    method: 'post',
    data: data
  })
}

// 修改SysRoom
export function updateSysRoom(data) {
  return request({
    url: '/api/v1/sysroom/' + data.ID,
    method: 'put',
    data: data
  })
}

// 删除SysRoom
export function delSysRoom(data) {
  return request({
    url: '/api/v1/sysroom',
    method: 'delete',
    data: data
  })
}

